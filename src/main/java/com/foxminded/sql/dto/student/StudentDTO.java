package com.foxminded.sql.dto.student;

import lombok.Data;

@Data
public class StudentDTO {
	private Integer studentId;
	private Integer groupId;
	private String firstName;
	private String lastName;
}
