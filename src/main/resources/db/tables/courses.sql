DROP SEQUENCE IF EXISTS seq_courses;

CREATE SEQUENCE seq_courses
START WITH 1
INCREMENT BY 1;

DROP TABLE IF EXISTS courses CASCADE;

CREATE TABLE courses
(
	course_id INT PRIMARY KEY ,
	course_name VARCHAR(50) NOT NULL,
	course_description VARCHAR(255) NOT NULL,
	UNIQUE (course_id)
);

INSERT INTO courses(course_id, course_name, course_description)
	VALUES
	(nextval('seq_courses'), 'Biology', 'studying the structure of living organism'),
	(nextval('seq_courses'), 'Math', 'learning math calculations'),
	(nextval('seq_courses'), 'Languages', 'studying languages'),
	(nextval('seq_courses'), 'Physics', 'learning physics'),
	(nextval('seq_courses'), 'Strength of materials', 'studying the structure of living organism'),
	(nextval('seq_courses'), 'Metal science', 'learning inner structure of different metal'),
	(nextval('seq_courses'), 'Production technology', 'learning technology of production'),
	(nextval('seq_courses'), 'Production automation', 'learning CNC machine, G-code'),
	(nextval('seq_courses'), 'Standardization', 'learning world standard of production part'),
	(nextval('seq_courses'), 'Drawing', 'learning to draw drawing of machine');
