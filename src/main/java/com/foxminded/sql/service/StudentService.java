package com.foxminded.sql.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.sql.dao.daoimpl.studentdao.StudentDaoImpl;
import com.foxminded.sql.domain.student.Student;
import com.foxminded.sql.dto.student.StudentDTO;
import com.foxminded.sql.exception.EntityNotFoundException;
import com.foxminded.sql.exception.EntitySaveException;
import com.foxminded.sql.exception.EntityUpdateException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StudentService {
	private ModelMapper modelMapper = new ModelMapper();
	private StudentDaoImpl dao;

	public StudentService(StudentDaoImpl dao) {
		this.dao = dao;
	}

	public List<StudentDTO> getAllStudent() {
		log.traceEntry();
		log.traceExit();
		return studentsToStudentsDTO(dao.getAll());
	}

	public StudentDTO createStudent(StudentDTO create) {
		log.traceEntry(create.toString());
		
		Student student = modelMapper.map(create, Student.class);
		Student response = dao.save(student).orElseThrow(() -> new EntitySaveException(Student.class));
		
		log.traceExit();
		return modelMapper.map(response, StudentDTO.class);
	}

	public StudentDTO updateStudent(StudentDTO update) {
		log.traceEntry(update.toString());
		
		Student student = modelMapper.map(update, Student.class);
		Student response = dao.update(student).orElseThrow(() -> new EntityUpdateException(Student.class, update.getStudentId()));
		
		log.traceExit();
		return modelMapper.map(response, StudentDTO.class);
	}

	public StudentDTO getStudent(int id) {
		log.traceEntry(id + "");
		Student response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Student.class, id));
		log.traceExit();
		return modelMapper.map(response, StudentDTO.class);
	}

	public void deleteStudent(int id) {
		log.traceEntry(id + "");
		dao.delete(id);
		log.traceExit();
	}

	public List<StudentDTO> getAllStudentsFromCourse(int courseId) {
		log.traceEntry(courseId + "");
		log.traceExit();
		return studentsToStudentsDTO(dao.getAllStudentsFromCourse(courseId));
	}

	public void addStudentsToCourse(int courseId, String students) {
		log.traceEntry(courseId + students);
		
		List<Student> studentsEntity = createStudentsList(students);
		
		dao.addStudentsToCourse(courseId, studentsEntity);
		
		log.traceExit();
	}
	
	public void deleteStudentFromCourse(int courseId, int studentId) {
		log.traceEntry(courseId + " " + studentId);
		dao.removeStudentFromCourse(studentId, courseId);
		log.traceExit();
	}
	
	public List<StudentDTO> getAllStudentsFromCourseByName(String courseName) {
		log.traceEntry(courseName);
		log.traceExit();
		return studentsToStudentsDTO(dao.getAllStudentsFromCourseByName(courseName));
		
	}
	
	private List<Student> createStudentsList(String studentIdLine) {
		
		String[] ids = studentIdLine.split(";");
														
		return idsToListEntity(ids);
		
	}
	
	private List<Student> idsToListEntity(String[] ids) {
		List<Student>  students = new ArrayList<>();
		for (String id : ids) {
			Student student = new Student();
			student.setStudentId(Integer.parseInt(id));
			students.add(student);
		}
		return students;
	}
	
	private List<StudentDTO> studentsToStudentsDTO(List<Student> students) {
		log.traceEntry(students.toString());
		
		List<StudentDTO> studentsDto = students.stream()
				.map(source -> modelMapper.map(source, StudentDTO.class)).collect(Collectors.toList());
		
		log.traceExit();
		return studentsDto;
	}
}
