package com.foxminded.sql.exception;

public class EntityUpdateException extends RuntimeException {
	public EntityUpdateException(Class entityClass, int id) {
		super(String.format("Entity %s with id=%d, can't update", entityClass, id));
	}
}
