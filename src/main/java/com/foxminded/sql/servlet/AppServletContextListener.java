package com.foxminded.sql.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.foxminded.sql.service.support.ConnectionService;
import com.foxminded.sql.service.support.CreateTablesService;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class AppServletContextListener implements  ServletContextListener{

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		CreateTablesService table = new CreateTablesService(new ConnectionService());
		table.refreshDatabase();
		log.info("Apllication was run");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		log.info("Apllication was closed");
	}
}
