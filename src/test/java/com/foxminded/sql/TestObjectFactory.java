package com.foxminded.sql;

import com.foxminded.sql.dao.daoimpl.coursedao.CourseDaoImpl;
import com.foxminded.sql.dao.daoimpl.groupdao.GroupDaoImpl;
import com.foxminded.sql.dao.daoimpl.studentdao.StudentDaoImpl;
import com.foxminded.sql.domain.course.Course;
import com.foxminded.sql.domain.group.Group;
import com.foxminded.sql.domain.student.Student;
import com.foxminded.sql.dto.courses.CourseDTO;
import com.foxminded.sql.dto.groups.GroupDTO;
import com.foxminded.sql.dto.student.StudentDTO;
import com.foxminded.sql.service.support.ConnectionService;

public class TestObjectFactory {
	private StudentDaoImpl studentDao = new StudentDaoImpl(new ConnectionService());
	private CourseDaoImpl courseDao = new CourseDaoImpl(new ConnectionService());
	private GroupDaoImpl groupDao = new GroupDaoImpl(new ConnectionService());
	
	public Student getStudent() {
		Group group = getGroup();
		
		Student student = new Student();
		student.setGroupId(group.getGroupId());
		student.setFirstName("testName");
		student.setLastName("testlastname");
		
		Student response = studentDao.save(student).get();
		return studentDao.get(response.getStudentId()).get();
	}
	
	public Group getGroup() {
		Group group = new Group();
		group.setGroupName("TD-10-2");
		
		Group response = groupDao.save(group).get();
		return groupDao.get(response.getGroupId()).get();
	}
	
	public Course getCourse() {
		Course course = new Course();
		course.setCourseName("English");
		course.setCourseDescription("Learn English");
		
		Course response = courseDao.save(course).get();
		System.out.println(response.getCourseId());
		return courseDao.get(response.getCourseId()).get();
	}
	
	public StudentDTO getStudentDTO() {
		Group group = getGroup();
		
		StudentDTO student = new StudentDTO();
		student.setGroupId(group.getGroupId());
		student.setFirstName("testName");
		student.setLastName("testlastname");
		return student;
	}
	
	public GroupDTO getGroupDTO() {
		GroupDTO group = new GroupDTO();
		group.setGroupName("test name");
		return group;
	}
	
	public CourseDTO getCourseDTO() {
		CourseDTO course = new CourseDTO();
		course.setCourseName("English");
		course.setCourseDescription("Learn English");
		return course;
	}
}
