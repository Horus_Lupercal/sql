package com.foxminded.sql.dto.groups;

import lombok.Data;

@Data
public class GroupDTO {
	private Integer groupId;
	private String groupName;
}
