package com.foxminded.sql.service.support;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ConnectionService {
	
	public Connection getConnection() {
		Connection connectionDB = null;
		Properties property = getProperties();
		try {
			Class.forName(property.getProperty("DataBaseDriver"));
		} catch (Exception e) {
			log.catching(e);
		}
		try {
			connectionDB = DriverManager.getConnection(property.get("DataBaseSource").toString(),
					property.get("login").toString(), property.get("password").toString());
			return connectionDB;
		} catch (Exception e) {
			log.catching(e);
		}
		return connectionDB;
	}

	private Properties getProperties() {
		Properties property = new Properties();
		try (InputStream in = new FileInputStream(getResourse())) {
			property.load(in);
		} catch (IOException e) {
			log.catching(e);
		}
		return property;
	}

	private String getResourse() {
		Path path = null;
		try {
			path = Paths.get(getClass().getClassLoader().getResource("application.properties").toURI());
		} catch (URISyntaxException e) {
			log.error("Error with reading resources", e);
			log.catching(e);
		}
		if (path == null) {
			return "";
		}
		return path.toString();
	}
}
