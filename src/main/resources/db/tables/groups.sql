DROP SEQUENCE IF EXISTS seq_groups;

CREATE SEQUENCE seq_groups
START WITH 1
INCREMENT BY 1;

DROP TABLE IF EXISTS groups CASCADE;

CREATE TABLE groups
(
	group_id INT PRIMARY KEY,
	group_name VARCHAR(30) NOT NULL,
	UNIQUE (group_id)
	
);

INSERT INTO groups(group_id, group_name)
	VALUES
	(nextval('seq_groups'), 'TB-10'),
	(nextval('seq_groups'), 'TB-12'),
	(nextval('seq_groups'), 'TA-10'),
	(nextval('seq_groups'), 'TA-12'),
	(nextval('seq_groups'), 'TC-10'),
	(nextval('seq_groups'), 'TC-12'),
	(nextval('seq_groups'), 'TG-10'),
	(nextval('seq_groups'), 'TG-12'),
	(nextval('seq_groups'), 'BA-10'),
	(nextval('seq_groups'), 'BA-12');