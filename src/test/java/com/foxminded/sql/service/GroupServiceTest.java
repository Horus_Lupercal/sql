package com.foxminded.sql.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.foxminded.sql.TestApllicationClass;
import com.foxminded.sql.TestObjectFactory;
import com.foxminded.sql.dao.daoimpl.groupdao.GroupDaoImpl;
import com.foxminded.sql.dto.groups.GroupDTO;
import com.foxminded.sql.exception.EntityNotFoundException;
import com.foxminded.sql.service.support.ConnectionService;

public class GroupServiceTest extends TestApllicationClass {
	private GroupService service = new GroupService(new GroupDaoImpl(new ConnectionService()));
	private TestObjectFactory objectFactory = new TestObjectFactory();

	@Test
	public void testGetGroupWithWrongIdAndExceptedException() {
		Assertions.assertThrows(EntityNotFoundException.class, () -> service.getGroup(200));
	}

	@Test
	public void TestGetGroupWithCorrectIdValueAndExceptedIsOk() {
		GroupDTO dao = service.getGroup(10);
		assertNotNull(dao);
	}

	@Test
	public void testDeleteGroup() {
		int id = 1;
		service.deleteGroup(id);
		Assertions.assertThrows(EntityNotFoundException.class, () -> service.getGroup(id));
	}

	@Test
	public void testGetAllGroups() {
		GroupDTO group1 = objectFactory.getGroupDTO();
		GroupDTO group2 = objectFactory.getGroupDTO();
		service.createGroup(group1);
		service.createGroup(group2);

		List<GroupDTO> Groups = service.getAllGroup();
		Assert.assertTrue(Groups.size() == 2);
	}

	@Test
	public void testCreateCursesAndExceptedIsOk() {
		GroupDTO group = objectFactory.getGroupDTO();
		GroupDTO response = service.createGroup(group);
		Assert.assertEquals(group.getGroupName(), response.getGroupName());
	}

	@Test
	public void testUpdateGroupsAndExceptedisOk() {
		int id = 1;
		GroupDTO group = objectFactory.getGroupDTO();
		GroupDTO response = service.updateGroup(id, group);
		Assert.assertEquals(group.getGroupName(), response.getGroupName());
	}
}
