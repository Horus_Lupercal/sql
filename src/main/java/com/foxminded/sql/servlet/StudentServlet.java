package com.foxminded.sql.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.sql.dao.daoimpl.studentdao.StudentDaoImpl;
import com.foxminded.sql.dto.student.StudentDTO;
import com.foxminded.sql.service.StudentService;
import com.foxminded.sql.service.support.ConnectionService;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StudentServlet extends HttpServlet {	
	private StudentService service = new StudentService(new StudentDaoImpl(new ConnectionService()));
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getQueryString();
		String courseId = req.getParameter("courseId");
		String studentId = req.getParameter("studentId");
		String groupId = req.getParameter("groupId");
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String courseName = req.getParameter("name");
		String studentIdLine = req.getParameter("studentIdLine");
		
		resp.setContentType("text/html");
		
		log.debug("Debug: response line" + action);
		
		try (PrintWriter out = resp.getWriter()) {
			out.print("<html><body>");

			if (action.contains("find")) {
				int id = Integer.parseInt(studentId);
				log.traceEntry("find by Id:" + id);
				StudentDTO response = service.getStudent(id);
				log.traceEntry(response.toString());
				out.print(toHtml(response));

			} else if (action.contains("showAll")) {

				List<StudentDTO> response = service.getAllStudent();
				StudentDTO[] courses = new StudentDTO[response.size()];
				log.traceEntry(response.toString());
				out.print(toHtml(response.toArray(courses)));

			} else if (action.contains("delete")) {
				int id = Integer.parseInt(studentId);
				service.deleteStudent(id);
				log.traceEntry("delete entity" + id);
				out.print(String.format("Entity with id:%s was deleted", studentId));

			} else if (action.contains("save")) {
				
				int groupIdInt = Integer.parseInt(groupId);
				StudentDTO create = toDto(null, groupIdInt, firstName, lastName);
				StudentDTO response = service.createStudent(create);
				log.traceEntry(response.toString());
				out.print(toHtml(response));

			} else if (action.contains("update")) {
				
				int id = Integer.parseInt(studentId);
				int groupIdInt = Integer.parseInt(groupId);
				StudentDTO update = toDto(id, groupIdInt, firstName, lastName);
				StudentDTO response = service.updateStudent(update);
				log.traceEntry("Entity update" + response.toString());
				out.print(toHtml(response));
				
			}  else if (action.contains("fromcourse")) {
				
				int id = Integer.parseInt(courseId);
				List<StudentDTO> response = service.getAllStudentsFromCourse(id);
				StudentDTO[] courses = new StudentDTO[response.size()];
				log.traceEntry(response.toString());
				out.print(toHtml(response.toArray(courses)));

			} else if (action.contains("delecourse")) {
				
				int studId = Integer.parseInt(studentId);
				int coursId = Integer.parseInt(courseId);
				service.deleteStudentFromCourse(coursId, studId);
				out.print(String.format("Student with id:%s from cousrse %s was deleted", studentId, courseId));
			
			} else if (action.contains("name")) {
				
				List<StudentDTO> response = service.getAllStudentsFromCourseByName(courseName);
				StudentDTO[] courses = new StudentDTO[response.size()];
				log.traceEntry(response.toString());
				out.print(toHtml(response.toArray(courses)));
				
			} else if (action.endsWith("listtocourse")) {
				
				int coursId = Integer.parseInt(courseId);
				
				service.addStudentsToCourse(coursId, studentIdLine);
				
				out.print(String.format("Students was added to course with id %d", coursId));
				
			}
			
			log.traceExit();
		} catch (Exception e) {
			log.error("Can't execute statement", e);
			log.catching(e);
        }
	}
	
	private StudentDTO toDto(Integer studentId, Integer groupId, String firstName, String lastName) {
		log.traceEntry(String.format("%d %d %s %s", studentId, groupId, firstName, lastName));
		StudentDTO dto = new StudentDTO();
		dto.setStudentId(studentId);
		dto.setGroupId(groupId);
		dto.setFirstName(firstName);
		dto.setLastName(lastName);
		return dto;
	}

	private String toHtml(StudentDTO... dtos) {
		StringBuilder result = new StringBuilder();
		result.append("<head>" + "<link rel='stylesheet' href='styles.css'>" + "</head>");
		result.append("<table style=\"width:100%\">");
		result.append("<tr>");
		result.append("<th>" + "id" + "</th>");
		result.append("<th>" + "groupId" + "</th>");
		result.append("<th>" + "firstName" + "</th>");
		result.append("<th>" + "LastName" + "</th>");
		for (StudentDTO dto : dtos) {
			result.append("<tr>");
			result.append("<th>" + dto.getStudentId() + "</th>");
			result.append("<th>" + dto.getGroupId() + "</th>");
			result.append("<th>" + dto.getFirstName() + "</th>");
			result.append("<th>" + dto.getLastName() + "</th>");
		}
		return result.toString();
	}
}
