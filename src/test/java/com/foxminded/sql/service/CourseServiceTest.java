package com.foxminded.sql.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.foxminded.sql.TestApllicationClass;
import com.foxminded.sql.TestObjectFactory;
import com.foxminded.sql.dao.daoimpl.coursedao.CourseDaoImpl;
import com.foxminded.sql.domain.course.Course;
import com.foxminded.sql.dto.courses.CourseDTO;
import com.foxminded.sql.exception.EntityNotFoundException;
import com.foxminded.sql.service.support.ConnectionService;

public class CourseServiceTest extends TestApllicationClass {
	private CourseService service = new CourseService(new CourseDaoImpl(new ConnectionService()));
	private TestObjectFactory objectFactory = new TestObjectFactory();
	
	@Test
	public void testGetCourseWithWrongIdAndExceptedException() {
		Assertions.assertThrows(EntityNotFoundException.class, () -> service.getCourse(200));
	}
	
	@Test 
	public void TestGetCourseWithCorrectIdValueAndExceptedIsOk() {
		CourseDTO dao = service.getCourse(10);
		Assert.assertNotNull(dao);
	}
	
	@Test
	public void testDeleteCourse() {
		int id = 1;
		service.deleteCourse(id);
		Assertions.assertThrows(EntityNotFoundException.class, () -> service.getCourse(id));
	}
	
	@Test
	public void testGetAllCourses() {
		List<CourseDTO> coursesBeferoSave = service.getAllCourse();
		int countbeforeSave = coursesBeferoSave.size();
		
		CourseDTO course1 = objectFactory.getCourseDTO();
		CourseDTO course2 = objectFactory.getCourseDTO();
		service.createCourse(course1);
		service.createCourse(course2);
		
		List<CourseDTO> coursesAfterSave = service.getAllCourse();
		int countAfterSave = coursesAfterSave.size();
		Assert.assertTrue(countbeforeSave < countAfterSave);
		Assert.assertTrue(countAfterSave - countbeforeSave == 2);
	}
	
	@Test
	public void testCreateCursesAndExceptedIsOk() {
		CourseDTO course = objectFactory.getCourseDTO();
		CourseDTO response = service.createCourse(course);
		Assert.assertNotNull(response.getCourseId());
	}
	
	@Test
	public void testUpdateCoursesAndExceptedisOk() {
		Course entityToUpdate = objectFactory.getCourse();
		CourseDTO course = objectFactory.getCourseDTO();
		CourseDTO response = service.updateCourse(entityToUpdate.getCourseId(), course);
		Assert.assertEquals(course.getCourseName(), response.getCourseName());
		Assert.assertEquals(course.getCourseDescription(), response.getCourseDescription());
	}
}
