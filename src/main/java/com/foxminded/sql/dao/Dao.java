package com.foxminded.sql.dao;

import java.util.List;
import java.util.Optional;

public interface Dao<T> {
	
	Optional<T> get(int id);
	
	List<T> getAll();
	
	Optional<T> save(T entity);
	
	Optional<T> update(T update);
	
	void delete(int id);
}
