package com.foxminded.sql.dao;

import java.util.List;

public interface StudentDao<T> extends Dao<T> {
	
	List<T> getAllStudentsFromCourse(int courseId);
		
	void removeStudentFromCourse(int studentId, int courseId);
	
	void addStudentsToCourse(int courseId, List<T> students);
	
	List<T> getAllStudentsFromCourseByName(String courseName);
}
