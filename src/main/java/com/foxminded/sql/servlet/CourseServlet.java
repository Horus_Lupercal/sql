package com.foxminded.sql.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.sql.dao.daoimpl.coursedao.CourseDaoImpl;
import com.foxminded.sql.dto.courses.CourseDTO;
import com.foxminded.sql.service.CourseService;
import com.foxminded.sql.service.support.ConnectionService;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CourseServlet extends HttpServlet {
	private CourseService service = new CourseService(new CourseDaoImpl(new ConnectionService()));
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getQueryString();
		String courseId = req.getParameter("courseId");
		String courseName = req.getParameter("courseName");
		String courseDescription = req.getParameter("courseDescription");
		resp.setContentType("text/html");

		try (PrintWriter out = resp.getWriter()) {
			out.print("<html><body>");

			if (action.contains("find")) {

				int id = Integer.parseInt(courseId);
				CourseDTO response = service.getCourse(id);
				out.print(toHtml(response));

			} else if (action.contains("showAll")) {

				List<CourseDTO> response = service.getAllCourse();
				CourseDTO[] courses = new CourseDTO[response.size()];
				out.print(toHtml(response.toArray(courses)));

			} else if (action.contains("delete")) {

				int id = Integer.parseInt(courseId);
				service.deleteCourse(id);
				out.print(String.format("Entity with id:%d was deleted", id));

			} else if (action.contains("save")) {

				CourseDTO response = service.createCourse(toDto(null, courseName, courseDescription));
				out.print(toHtml(response));

			} else if (action.contains("update")) {

				int id = Integer.parseInt(courseId);
				CourseDTO response = service.updateCourse(toDto(id, courseName, courseDescription));
				out.print(toHtml(response));

			}
			out.print("</html></body>");
		} catch (Exception e) {
			log.error("Can't execute statement", e);
			log.catching(e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	private CourseDTO toDto(Integer courseId, String courseName, String courseDescription) {
		CourseDTO dto = new CourseDTO();
		dto.setCourseId(courseId);
		dto.setCourseName(courseName);
		dto.setCourseDescription(courseDescription);
		return dto;
	}
	
	private String toHtml(CourseDTO... dtos) {
		StringBuilder result = new StringBuilder();
		result.append("<head>" + "<link rel='stylesheet' href='styles.css'>" + "</head>");
		result.append("<table style=\"width:100%\">");
		result.append("<tr>");
		result.append("<th>" + "id" + "</th>");
		result.append("<th>" + "name" + "</th>");
		result.append("<th>" + "description" + "</th>");
		for (CourseDTO dto : dtos) {
			result.append("<tr>");
			result.append("<th>" + dto.getCourseId() + "</th>");
			result.append("<th>" + dto.getCourseName() + "</th>");
			result.append("<th>" + dto.getCourseDescription() + "</th>");
		}
		return result.toString();
	}
}
