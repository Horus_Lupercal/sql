package com.foxminded.sql.dao;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

import com.foxminded.sql.TestApllicationClass;
import com.foxminded.sql.TestObjectFactory;
import com.foxminded.sql.dao.daoimpl.coursedao.CourseDaoImpl;
import com.foxminded.sql.domain.course.Course;
import com.foxminded.sql.service.support.ConnectionService;

public class CourseDaoTest extends TestApllicationClass  {
	private CourseDaoImpl dao = new CourseDaoImpl(new ConnectionService());
	private TestObjectFactory objectFactory = new TestObjectFactory();

	@Test
	public void testSaveCourseAndExceptedStatusOkey() {
		Course course = new Course();
		course.setCourseName("English");
		course.setCourseDescription("Learn English");

		Course result = dao.save(course).get();
				
		Assert.assertNotNull(result.getCourseId());
	}

	@Test
	public void testGetCourseAndExceptedStatusOkey() {
	
		Course result  = dao.get(1).get();
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testGetCourseByWrongIdAndExceptedException() {
		Optional<Course> course = dao.get(1200);
		Assert.assertTrue(!course.isPresent());
	}
	
	@Test
	public void testGetAllCourseFromDataBaseExceptedOk() {
		Course course1 = objectFactory.getCourse();
		dao.save(course1);
		Course course2 = objectFactory.getCourse();
		dao.save(course2);
		List<Course> list = dao.getAll();
		Assert.assertTrue(list.size() > 0);
	}
	
	@Test
	public void testUpdateCourseAndExceptedIsOk() {
		Course course = objectFactory.getCourse();
		Course update = new Course();
		update.setCourseName("test");
		update.setCourseDescription("test");
		Course response = dao.update(course.getCourseId(), update).get();
		Assertions.assertThat(update).isEqualToIgnoringGivenFields(response, "courseId");
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testDeleteCoursebyIdExceptedIsOk() {
		dao.delete(1);
		dao.get(1).get();
	}
}