package com.foxminded.sql.dao;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

import com.foxminded.sql.TestApllicationClass;
import com.foxminded.sql.TestObjectFactory;
import com.foxminded.sql.dao.daoimpl.groupdao.GroupDaoImpl;
import com.foxminded.sql.domain.group.Group;
import com.foxminded.sql.service.support.ConnectionService;

public class GroupDaoTest extends TestApllicationClass {
	private GroupDaoImpl dao = new GroupDaoImpl(new ConnectionService());
	private TestObjectFactory objectFactory = new TestObjectFactory();

	@Test
	public void testSaveGroupAndExceptedStatusOkey() {
		Group group = new Group();
		group.setGroupName("TD-10-2");
		Group response = dao.save(group).get();
		
		Group find = dao.get(response.getGroupId()).get();
		Assert.assertNotNull(response.getGroupId());
		Assertions.assertThat(group).isEqualToIgnoringGivenFields(find, "groupId");
	}

	@Test
	public void testGetGroupAndExceptedStatusOkey() {
		Group group = objectFactory.getGroup();

		Group result = dao.get(group.getGroupId()).get();
		Assert.assertNotNull(result);
	}

	@Test
	public void testGetGroupByWrongIdAndExceptedException() {
		Optional<Group> croup = dao.get(1200);
		Assert.assertTrue(!croup.isPresent());
	}

	@Test
	public void testGetAllGroupFromDataBaseExceptedOk() {
		objectFactory.getGroup();
		objectFactory.getGroup();
		List<Group> list = dao.getAll();
		Assert.assertTrue(list.size() > 0);
	}

	@Test
	public void testUpdateGroupAndExceptedIsOk() {
		Group group = objectFactory.getGroup();

		Group update = new Group();
		update.setGroupName("235813");

		Group updateId = dao.update(group.getGroupId(), update).get();
		Group result = dao.get(updateId.getGroupId()).get();
		Assertions.assertThat(update).isEqualToIgnoringGivenFields(result, "groupId");
	}

	@Test(expected = NoSuchElementException.class)
	public void testDeleteGroupbyIdExceptedIsOk() {
		dao.delete(1);
		dao.get(1).get();
	}

	@Test
	public void testfindEntitiesByCount() {
		List<Group> list = dao.findAllGroupsWithStudentsNumberMoreThan(20);
		Assert.assertTrue(list.size() > 0);
	}
}
