package com.foxminded.sql.exception;

public class EntityNotFoundException extends RuntimeException {
	
	public EntityNotFoundException(Class entityClass, int id) {
		super(String.format("Entity %s with id=%d not Found!", entityClass.getSimpleName(), id));
	}
}
