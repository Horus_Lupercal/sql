package com.foxminded.sql.dao;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

import com.foxminded.sql.TestApllicationClass;
import com.foxminded.sql.TestObjectFactory;
import com.foxminded.sql.dao.daoimpl.studentdao.StudentDaoImpl;
import com.foxminded.sql.domain.group.Group;
import com.foxminded.sql.domain.student.Student;
import com.foxminded.sql.service.support.ConnectionService;

public class StudentDaoTest extends TestApllicationClass {
	private StudentDaoImpl dao = new StudentDaoImpl(new ConnectionService());
	private TestObjectFactory objectFactory = new TestObjectFactory();
	
	@Test
	public void testSaveStudentAndExceptedStatusOkey() {
		Group group = objectFactory.getGroup();
		
		Student student = new Student();
		student.setGroupId(group.getGroupId());
		student.setFirstName("testName");
		student.setLastName("testlastname");
		
		Student result = dao.save(student).get();
				
		Assert.assertNotNull(result.getStudentId());
		Student entityAfterUpdate = dao.get(result.getStudentId()).get();
		Assertions.assertThat(student).isEqualToIgnoringGivenFields(entityAfterUpdate, "studentId");
	}

	@Test
	public void testGetStudentAndExceptedStatusOkey() {
		Student result  = dao.get(1).get();
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testGetStudentByWrongIdAndExceptedException() {
		Optional<Student> student = dao.get(1200);
		Assert.assertTrue(!student.isPresent());
	}
	
	@Test
	public void testGetAllStudentFromDataBaseExceptedOk() {
		Student student1 = objectFactory.getStudent();
		dao.save(student1);
		List<Student> list = dao.getAll();
		Assert.assertTrue(list.size() > 1);
	}
	
	@Test
	public void testUpdateStudentAndExceptedIsOk() {
		int id = 2;
		Student update = new Student();
		update.setGroupId(1);
		update.setFirstName("test");
		update.setLastName("testlastname");
		Student response = dao.update(id, update).get();
		Assertions.assertThat(update).isEqualToIgnoringGivenFields(response, "studentId");
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testDeleteStudentbyIdExceptedIsOk() {
		dao.delete(1);
		dao.get(1).get();
	}
	
	@Test
	public void testGetStudentFromCourse() {
		List<Student> students = dao.getAllStudentsFromCourse(2);
		Assert.assertTrue(students.size() > 0);
	}
		
	@Test
	public void testDeleteStudentFromCourse() {
		int studentId = 1;
		int courseId = 1;
		dao.removeStudentFromCourse(studentId, courseId);
		Optional<Student> student = dao.get(studentId);
		Assert.assertTrue(student.isPresent());
		List<Student> studentsFromCourse = dao.getAllStudentsFromCourse(courseId);
		Assert.assertTrue(!studentsFromCourse.contains(student.get()));
	}
	
	@Test
	public void testAddStudentsToCourse() {
		int courseId = 1;
		int sizeGroup = dao.getAllStudentsFromCourse(courseId).size();
		Student student = objectFactory.getStudent();
		student = dao.save(student).get();
		List<Student> students = Arrays.asList(student);
		dao.addStudentsToCourse(courseId, students);
		int sizeGroupAfterUpdate = dao.getAllStudentsFromCourse(courseId).size();
		Assert.assertTrue(sizeGroup < sizeGroupAfterUpdate);
		Assert.assertTrue(sizeGroupAfterUpdate - sizeGroup == 1);
	}
}
