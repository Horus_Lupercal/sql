package com.foxminded.sql.domain.group;

import lombok.Data;

@Data
public class Group {
	private Integer groupId;
	private String groupName;
}
