package com.foxminded.sql.dao;

import java.util.List;

public interface GroupDao<T> extends Dao<T> {
	
	List<T> findAllGroupsWithStudentsNumberMoreThan(int count);
}
