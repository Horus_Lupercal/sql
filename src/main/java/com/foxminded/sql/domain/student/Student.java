package com.foxminded.sql.domain.student;

import lombok.Data;

@Data
public class Student {
	private Integer studentId;
	private Integer groupId;
	private String firstName;
	private String lastName;
}
