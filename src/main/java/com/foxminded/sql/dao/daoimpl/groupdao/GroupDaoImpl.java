package com.foxminded.sql.dao.daoimpl.groupdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.foxminded.sql.dao.GroupDao;
import com.foxminded.sql.domain.group.Group;
import com.foxminded.sql.service.support.ConnectionService;

import lombok.Cleanup;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class GroupDaoImpl implements GroupDao<Group>  {
	private ConnectionService connectionService;

	public GroupDaoImpl(ConnectionService connectionService) {
		this.connectionService = connectionService;
	}

	@Override
	public Optional<Group> get(int id) {
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement("SELECT group_id, group_name FROM groups WHERE group_id = ?");
			
			statement.setInt(1, id);
			
			@Cleanup ResultSet response = statement.executeQuery();

			if (response.next()) {
				Group groups = new Group();
				groups.setGroupId(response.getInt("group_id"));
				groups.setGroupName(response.getString("group_name"));
				return Optional.of(groups);
			}
			
		} catch (SQLException e) {
			log.catching(e);
		}
		return Optional.empty();
	}

	@Override
	public List<Group> getAll() {
		List<Group> allGroups = new ArrayList<>();
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup Statement statement = conection.createStatement();
			@Cleanup ResultSet response = statement.executeQuery("SELECT group_id, group_name FROM groups");
	
			while (response.next()) {
				Group groups = new Group();
				groups.setGroupId(response.getInt("group_id"));
				groups.setGroupName(response.getString("group_name"));
				allGroups.add(groups);
			}
				
		} catch (SQLException e) {
			log.catching(e);
		}
		return allGroups;
	}

	@Override
	public Optional<Group> save(Group group) {
		try { 
			String quary = "INSERT INTO groups(group_id, group_name) VALUES (nextval('seq_groups'), ?);";
			
			@Cleanup Connection conection = connectionService.getConnection(); 
			@Cleanup PreparedStatement statement = conection.prepareStatement(quary, Statement.RETURN_GENERATED_KEYS);
	
			statement.setString(1, group.getGroupName());
			
			statement.executeUpdate();
			
			@Cleanup ResultSet generatedKeys = statement.getGeneratedKeys();
			
			if (generatedKeys.next()) {
				Group groups = new Group();
				groups.setGroupId(generatedKeys.getInt("group_id"));
				return Optional.of(groups);
			}
			
		} catch (SQLException e) {
			log.catching(e);
		} 
		return Optional.empty();
	}

	@Override
	public Optional<Group> update(Group update) {
		try { 
			String quary = "UPDATE groups SET group_name=? WHERE group_id=?;";
			
			@Cleanup Connection conection = connectionService.getConnection(); 
			@Cleanup PreparedStatement statement = conection.prepareStatement(quary);
		
			statement.setString(1, update.getGroupName());
			statement.setInt(2, update.getGroupId());
			
			int updatedRows = statement.executeUpdate();
			
			if (updatedRows > 0) {
				return Optional.of(update);
			}
			
			log.debug("Nothing to Update");
		} catch (SQLException e) {
			log.catching(e);
		}
		return Optional.empty();
	}

	@Override
	public void delete(int id) {
		try {
			@Cleanup Connection conection = connectionService.getConnection(); 
			@Cleanup PreparedStatement statement = conection.prepareStatement("DELETE FROM groups WHERE group_id=?");
			
			statement.setInt(1, id);
			
			statement.execute();

		} catch (SQLException e) {
			log.catching(e);
		}
	}
	
	@Override
	public List<Group> findAllGroupsWithStudentsNumberMoreThan(int count) {
		List<Group> allGroups = new ArrayList<>();
		try {
			@Cleanup Connection conection = connectionService.getConnection(); 
			@Cleanup PreparedStatement statement = conection.prepareStatement(
					"ELECT groups.group_id, groups.group_name FROM groups" + 
					"INNER JOIN students ON students.group_id = groups.group_id" + 
					"GROUP BY groups.group_id HAVING COUNT (groups.group_id) >= ?;");
			
			statement.setInt(1, count);
			
			@Cleanup ResultSet response = statement.executeQuery();
			
			while (response.next()) {
				Group groups = new Group();
				groups.setGroupId(response.getInt("group_id"));
				groups.setGroupName(response.getString("group_name"));
				allGroups.add(groups);
			}

		} catch (SQLException e) {
			log.catching(e);
		}
		return allGroups;
	}
}