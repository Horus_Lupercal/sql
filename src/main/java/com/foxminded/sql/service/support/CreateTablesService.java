package com.foxminded.sql.service.support;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Cleanup;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class CreateTablesService {
	private ConnectionService conectionServ;
	private String tableValues = "db\\data";

	public CreateTablesService(ConnectionService conectionServ) {
		this.conectionServ = conectionServ;
	}

	public CreateTablesService(ConnectionService conectionServ, String newValuesSource) {
		this.conectionServ = conectionServ;
		this.tableValues = newValuesSource;
	}

	public void refreshDatabase() {
		String courses = "DROP SEQUENCE IF EXISTS seq_courses;\r\n" 
					   + "CREATE SEQUENCE seq_courses\r\n"
					   + "START WITH 1\r\n" 
					   + "INCREMENT BY 1;\r\n" 
					   + "\r\n" 
					   + "DROP TABLE IF EXISTS courses CASCADE;\r\n"
					   + "\r\n" 
					   + "CREATE TABLE courses\r\n" 
					   + "(\r\n" 
					   + "	course_id INT PRIMARY KEY ,\r\n"
					   + "	course_name VARCHAR(50) NOT NULL,\r\n" 
					   + "	course_description VARCHAR(255) NOT NULL,\r\n"
					   + "	UNIQUE (course_id)\r\n" 
					   + ");\r\n";
					  
		String groups = "DROP SEQUENCE IF EXISTS seq_groups;" + "CREATE SEQUENCE seq_groups\r\n" + "START WITH 1\r\n"
					  + "INCREMENT BY 1;\r\n" 
					  + "\r\n" 
					  + "DROP TABLE IF EXISTS groups CASCADE;\r\n" 
					  + "\r\n"
					  + "CREATE TABLE groups\r\n" 
					  + "(\r\n" 
					  + "	group_id INT PRIMARY KEY,\r\n"
					  + "	group_name VARCHAR(30) NOT NULL,\r\n" 
					  + "	UNIQUE (group_id)\r\n" 
					  + ");\r\n";
					 
		String students = "DROP SEQUENCE IF EXISTS seq_students;\r\n" 
						+ "\r\n"
						+ "CREATE SEQUENCE IF NOT EXISTS seq_students\r\n" 
						+ "START WITH 1\r\n" 
						+ "INCREMENT BY 1;\r\n" 
						+ "\r\n"
						+ "DROP TABLE IF EXISTS students CASCADE;\r\n" 
						+ "\r\n" 
						+ "CREATE TABLE students\r\n" 
						+ "( \r\n"
						+ "	student_id INT NOT NULL,\r\n" 
						+ "	group_id INT REFERENCES groups(group_id) ON DELETE CASCADE,\r\n"
						+ "	first_name  VARCHAR(30) NOT NULL,\r\n"
						+ "	last_name  VARCHAR(30) NOT NULL,\r\n"
						+ "	PRIMARY KEY (student_id, group_id),\r\n" 
						+ "	UNIQUE (student_id)\r\n" + ");\r\n";
					
		String studentsCourses =  "DROP TABLE IF EXISTS students_courses CASCADE;\r\n" 
								+ "\r\n"
								+ "CREATE TABLE students_courses\r\n" 
								+ "(\r\n"
								+ "	student_id INT REFERENCES students(student_id) ON DELETE CASCADE,\r\n"
								+ "	course_id INT REFERENCES courses(course_id) ON DELETE CASCADE,\r\n"
								+ "	CONSTRAINT students_courses_pkey PRIMARY KEY (student_id, course_id)\r\n" 
								+ ");\r\n"
								+ "GRANT ALL ON SCHEMA public TO owner_sql_jdbc_school_db;";

		sendRequest(courses, groups, students, studentsCourses);
		insertTablesValues();
	}
		

	private void insertTablesValues() {
		try {
			Path path = Paths.get(getClass().getClassLoader().getResource(tableValues).toURI());
			for (File file : path.toFile().listFiles()) {
				if (file.isFile() && file.getName().endsWith(".db")) {

					@Cleanup Stream<String> streamFromFile = Files.lines(file.toPath());
					
					List<String[]> value = streamFromFile.map(line -> line.split(";")).collect(Collectors.toList());
					
					if (!value.isEmpty()) {
						String equary = createQuary(value, file.getName());
						sendRequest(equary);
					}
				}
			}
		} catch (Exception e) {
			log.catching(e);
		}
	}

	private String createQuary(List<String[]> value, String tableName) {
		String table = tableName.replaceAll(".db", "");
		StringBuilder quary = new StringBuilder("INSERT INTO ".concat(table));

		for (int i = 0; i < value.size(); i++) {
			quary.append(createValueLine(value.get(i)));
			if (i == 0) {
				quary.append("\r\n");
				quary.append("VALUES");
				quary.append("\r\n");
			} else if (i == value.size() - 1){
				quary.append(";");
			} else {
				quary.append(",\r\n");
			}
		}
		return quary.toString();
	}

	private String createValueLine(String[] quaryValues) {
		StringBuilder valueLine = new StringBuilder("(");
		for (int i = 0; i < quaryValues.length; i++) {
			valueLine.append(quaryValues[i]);
			if (quaryValues.length > 1 && i != quaryValues.length - 1) {
				valueLine.append(", ");
			}
		}
		valueLine.append(")");
		return valueLine.toString();
	}

	private void sendRequest(String... requests) {
		for (String request : requests) {
			try (Connection connectionDB = conectionServ.getConnection();
					Statement statement = connectionDB.createStatement()) {

				statement.execute(request);

			} catch (SQLException e) {
				log.catching(e);
			}
		}
	}
}
