package com.foxminded.sql.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.sql.dao.daoimpl.groupdao.GroupDaoImpl;
import com.foxminded.sql.domain.group.Group;
import com.foxminded.sql.dto.groups.GroupDTO;
import com.foxminded.sql.exception.EntityNotFoundException;
import com.foxminded.sql.exception.EntitySaveException;
import com.foxminded.sql.exception.EntityUpdateException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GroupService {
	private ModelMapper modelMapper = new ModelMapper();
	private GroupDaoImpl dao;

	public GroupService(GroupDaoImpl dao) {
		this.dao = dao;
	}

	public List<GroupDTO> getAllGroup() {
		log.traceEntry();
		log.traceExit();
		return groupsToGroupsDTO(dao.getAll());
	}

	public GroupDTO createGroup(GroupDTO create) {
		log.traceEntry(create.toString());
		
		Group group = modelMapper.map(create, Group.class);
		Group response = dao.save(group).orElseThrow(() -> new EntitySaveException(Group.class));
		
		log.traceExit();
		return modelMapper.map(response, GroupDTO.class);
	}

	public GroupDTO updateGroup(GroupDTO update) {
		log.traceEntry(update.toString());
		
		Group group = modelMapper.map(update, Group.class);
		Group response = dao.update(group).orElseThrow(() -> new EntityUpdateException(Group.class, update.getGroupId()));
		
		log.traceExit();
		return modelMapper.map(response, GroupDTO.class);
	}

	public GroupDTO getGroup(int id) {
		log.traceEntry(id + "");
		
		Group response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Group.class, id));
		
		log.traceExit();
		return modelMapper.map(response, GroupDTO.class);
	}

	public void deleteGroup(int id) {
		log.traceEntry(id + "");
		dao.delete(id);
		log.traceExit();
	}
	
	public List<GroupDTO> findGroupByCountOfStudents(int count) {
		log.traceEntry(count + "");
		log.traceExit();
		return groupsToGroupsDTO(dao.findAllGroupsWithStudentsNumberMoreThan(count));
	}
	
	private List<GroupDTO> groupsToGroupsDTO(List<Group> groups) {
		log.traceEntry(groups.toString());
		
		List<GroupDTO> groupsDTO = groups.stream().map(source -> modelMapper.map(source, GroupDTO.class))
				.collect(Collectors.toList());
		
		log.traceEntry(groupsDTO.toString());
		log.traceExit();
		return groupsDTO;
	}
}
