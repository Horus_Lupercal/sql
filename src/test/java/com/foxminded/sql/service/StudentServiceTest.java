package com.foxminded.sql.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import com.foxminded.sql.TestApllicationClass;
import com.foxminded.sql.TestObjectFactory;
import com.foxminded.sql.dao.daoimpl.studentdao.StudentDaoImpl;
import com.foxminded.sql.domain.group.Group;
import com.foxminded.sql.dto.student.StudentDTO;
import com.foxminded.sql.exception.EntityNotFoundException;
import com.foxminded.sql.service.support.ConnectionService;

public class StudentServiceTest extends TestApllicationClass {
	private StudentService service = new StudentService(new StudentDaoImpl(new ConnectionService()));
	private TestObjectFactory objectFactory = new TestObjectFactory();
	
	@Test
	public void testGetStudentWithWrongIdAndExceptedException() {
		Assertions.assertThrows(EntityNotFoundException.class, () -> service.getStudent(2000));
	}

	@Test
	public void TestGetStudentWithCorrectIdValueAndExceptedIsOk() {
		StudentDTO dao = service.getStudent(10);
		assertNotNull(dao);
	}

	@Test
	public void testDeleteStudent() {
		int id = 1;
		service.deleteStudent(id);
		Assertions.assertThrows(EntityNotFoundException.class, () -> service.getStudent(id));
	}

	@Test
	public void testGetAllStudents() {
		StudentDTO student1 = objectFactory.getStudentDTO();
		StudentDTO student2 = objectFactory.getStudentDTO();
		service.createStudent(student1);
		service.createStudent(student2);

		List<StudentDTO> students = service.getAllStudent();
		Assert.assertTrue(students.size() > 0);
	}

	@Test
	public void testCreateStudentAndExceptedIsOk() {
		Group group = objectFactory. getGroup();
		StudentDTO student = new StudentDTO();
		student.setGroupId(group.getGroupId());
		student.setFirstName("testName");
		student.setLastName("testlastname");
		
		StudentDTO response = service.createStudent(student);
		
		assertNotNull(response.getStudentId());
	}

	@Test
	public void testUpdateStudentsAndExceptedisOk() {
		int id = 1;
		StudentDTO student = objectFactory.getStudentDTO();
		StudentDTO response = service.updateStudent(id, student);
		Assert.assertEquals(student.getFirstName(), response.getFirstName());
		Assert.assertEquals(student.getLastName(), response.getLastName());
		Assert.assertEquals(student.getGroupId(), response.getGroupId());
	}
	
	@Test
	public void testGetStudentFromCourse() {
		List<StudentDTO> students = service.getAllStudentsFromCourse(2);
		Assert.assertTrue(students.size() > 0);
	}
	
	@Test
	public void testDeleteStudentFromCourse() {
		int studentId = 1;
		int courseId = 1;
		service.deleteStudentFromCourse(studentId, courseId);
		StudentDTO student = service.getStudent(studentId);
		List<StudentDTO> studentsFromCourse = service.getAllStudentsFromCourse(courseId);
		Assert.assertTrue(!studentsFromCourse.contains(student));
	}
	
	@Test
	public void testAddStudentsToCourse() {
		int courseId = 5;
		int sizeGroup = service.getAllStudentsFromCourse(courseId).size();
		
		StudentDTO student = objectFactory.getStudentDTO();
		student = service.createStudent(student);
		String students = "1;2;3";
		service.addStudentsToCourse(courseId, students);
		int sizeGroupAfterUpdate = service.getAllStudentsFromCourse(courseId).size();
		
		Assert.assertTrue(sizeGroupAfterUpdate - sizeGroup == 3);
	}
}
