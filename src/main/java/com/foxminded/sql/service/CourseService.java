package com.foxminded.sql.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.sql.dao.daoimpl.coursedao.CourseDaoImpl;
import com.foxminded.sql.domain.course.Course;
import com.foxminded.sql.dto.courses.CourseDTO;
import com.foxminded.sql.exception.EntityNotFoundException;
import com.foxminded.sql.exception.EntitySaveException;
import com.foxminded.sql.exception.EntityUpdateException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CourseService {
	private ModelMapper modelMapper = new ModelMapper();
	private CourseDaoImpl dao;

	public CourseService(CourseDaoImpl dao) {
		this.dao = dao;
	}

	public List<CourseDTO> getAllCourse() {		
		List<CourseDTO> courses = dao.getAll().stream().map(source -> modelMapper.map(source, CourseDTO.class))
				.collect(Collectors.toList());
		
		log.traceEntry(courses.toString());
		log.traceExit();
		return courses;
	}

	public CourseDTO createCourse(CourseDTO create) {
		log.traceEntry(create.toString());
		
		Course course = modelMapper.map(create, Course.class);
		Course response = dao.save(course).orElseThrow(() -> new EntitySaveException(Course.class));
		
		log.traceExit();
		return modelMapper.map(response, CourseDTO.class);
	}

	public CourseDTO updateCourse(CourseDTO update) {
		log.traceEntry(update.toString());
		
		Course course = modelMapper.map(update, Course.class);
		Course response = dao.update(course).orElseThrow(() -> new EntityUpdateException(Course.class, update.getCourseId()));
		
		log.traceExit();
		return modelMapper.map(response, CourseDTO.class);
	}

	public CourseDTO getCourse(int id) {
		log.traceEntry(id + " ");
		
		Course response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Course.class, id));
		
		log.traceExit();
		return modelMapper.map(response, CourseDTO.class);
	}

	public void deleteCourse(int id) {
		log.traceEntry(id + " ");
		dao.delete(id);
		log.traceExit();
	}
}
