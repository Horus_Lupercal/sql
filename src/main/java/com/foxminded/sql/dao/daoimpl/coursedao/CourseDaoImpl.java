package com.foxminded.sql.dao.daoimpl.coursedao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.foxminded.sql.dao.Dao;
import com.foxminded.sql.domain.course.Course;
import com.foxminded.sql.service.support.ConnectionService;

import lombok.Cleanup;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class CourseDaoImpl implements Dao<Course> {
	private ConnectionService connectionService;

	public CourseDaoImpl(ConnectionService connectionService) {
		this.connectionService = connectionService;
	}

	@Override
	public Optional<Course> get(int id) {
		try {
			log.traceEntry(id + "");
			
			@Cleanup Connection conection = connectionService.getConnection();;
			@Cleanup PreparedStatement statement = conection.prepareStatement("SELECT course_id, course_name, course_description FROM courses WHERE course_id = ?");
			
			statement.setInt(1, id);
			
			@Cleanup ResultSet response = statement.executeQuery();
				
				if (response.next()) {
					Course courses = new Course();
					courses.setCourseId(response.getInt("course_id"));
					courses.setCourseName(response.getString("course_name"));
					courses.setCourseDescription(response.getString("course_description"));
					return Optional.of(courses);
				}
				
		} catch(SQLException e) {
			log.catching(e);
		}
		log.traceExit();
		return Optional.empty();
	}

	@Override
	public List<Course> getAll() {
		List<Course> allCourses = new ArrayList<>();
		try {
			
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup Statement statement = conection.createStatement();
			@Cleanup ResultSet response = statement.executeQuery("SELECT course_id, course_name, course_description FROM courses");
			
				while (response.next()) {
					Course courses = new Course();
					courses.setCourseId(response.getInt("course_id"));
					courses.setCourseName(response.getString("course_name"));
					courses.setCourseDescription(response.getString("course_description"));
					allCourses.add(courses);
				}
				
		} catch (SQLException e) {
			log.catching(e);
		}
		log.traceEntry(allCourses.toString());
		log.traceExit();
		return allCourses;
	}

	@Override
	public Optional<Course> save(Course course) {
		log.traceEntry(course.toString());
		try {
			String quary = "INSERT INTO courses(course_id, course_name, course_description) VALUES (nextval('seq_courses'), ?, ?);";
			
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement(quary, Statement.RETURN_GENERATED_KEYS);
					
			statement.setString(1, course.getCourseName());
			statement.setString(2, course.getCourseDescription());
			
			statement.executeUpdate();
					
			@Cleanup ResultSet generatedKeys = statement.getGeneratedKeys();
	
			if (generatedKeys.next()) {
				Course courses = new Course();
				courses.setCourseId(generatedKeys.getInt("course_id"));
				return Optional.of(courses);
			}
			
		} catch (SQLException e) {
			log.catching(e);
		} 
		log.traceExit();
		return Optional.empty();
	}

	@Override
	public Optional<Course> update(Course update) {		
		log.traceEntry(update.toString());
		try {
			String quary = "UPDATE courses SET course_name=?, course_description=? WHERE course_id=?;";
					
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement(quary);
			
			statement.setString(1, update.getCourseName());
			statement.setString(2, update.getCourseDescription());
			statement.setInt(3, update.getCourseId());

			int updatedRows = statement.executeUpdate();
			
			if (updatedRows > 0) {
				return Optional.of(update);
			}
			log.info("Nothing to Update");
		} catch (SQLException e) {
			log.catching(e);
		} 
		log.traceExit();
		return Optional.empty();
	}

	@Override
	public void delete(int id) {
		log.traceEntry(id + " ");
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement("DELETE FROM courses WHERE course_id=?");

			statement.setInt(1, id);
			statement.execute();

		} catch (SQLException e) {
			log.catching(e);
		}
		log.traceExit();
	}
}
