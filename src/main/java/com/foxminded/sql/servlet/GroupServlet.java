package com.foxminded.sql.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.foxminded.sql.dao.daoimpl.groupdao.GroupDaoImpl;
import com.foxminded.sql.dto.groups.GroupDTO;
import com.foxminded.sql.service.GroupService;
import com.foxminded.sql.service.support.ConnectionService;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GroupServlet extends HttpServlet {
	private GroupService service = new GroupService(new GroupDaoImpl(new ConnectionService()));
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getQueryString();
		String groupId = req.getParameter("groupId");
		String groupName = req.getParameter("groupName");
		String studentCount = req.getParameter("count");
		resp.setContentType("text/html");
		
		
		try (PrintWriter out = resp.getWriter()) {
			
			out.print(action);
			if (action.contains("find")) {

				int id = Integer.parseInt(groupId);
				GroupDTO response = service.getGroup(id);
				out.print(toHtml(response));

			} else if (action.contains("showAll")) {

				List<GroupDTO> response = service.getAllGroup();
				GroupDTO[] courses = new GroupDTO[response.size()];
				out.print(toHtml(response.toArray(courses)));

			} else if (action.contains("delete")) {

				int id = Integer.parseInt(groupId);
				service.deleteGroup(id);
				out.print(String.format("Entity with id:%d was deleted", id));

			} else if (action.endsWith("save")) {
				
				GroupDTO create = toDto(null, groupName);
				GroupDTO response = service.createGroup(create);	
				out.print(toHtml(response));

			} else if (action.contains("update")) {

				int id = Integer.parseInt(groupId);
				GroupDTO update = toDto(id, groupName);
				GroupDTO response = service.updateGroup(update);
				out.print(toHtml(response));

			} else if (action.contains("groupbycount")) {
				
				int count = Integer.parseInt(studentCount);
				List<GroupDTO> response = service.findGroupByCountOfStudents(count);
				GroupDTO[] courses = new GroupDTO[response.size()];
				out.print(toHtml(response.toArray(courses)));
				
			}
			
			out.print("</html></body>");
		} catch (Exception e) {
			log.error("Can't execute statement", e);
			log.catching(e);
        }
	}

	private GroupDTO toDto(Integer groupId, String groupName) {
		GroupDTO dto = new GroupDTO();
		dto.setGroupId(groupId);
		dto.setGroupName(groupName);
		return dto;
	}

	private String toHtml(GroupDTO... dtos) {
		StringBuilder result = new StringBuilder();
		result.append("<head>" + "<link rel='stylesheet' href='styles.css'>" + "</head>");
		result.append("<table style=\"width:100%\">");
		result.append("<tr>");
		result.append("<th>" + "id" + "</th>");
		result.append("<th>" + "name" + "</th>");
		for (GroupDTO dto : dtos) {
			result.append("<tr>");
			result.append("<th>" + dto.getGroupId() + "</th>");
			result.append("<th>" + dto.getGroupName() + "</th>");
		}
		return result.toString();
	}
}
