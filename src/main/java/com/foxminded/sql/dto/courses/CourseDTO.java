package com.foxminded.sql.dto.courses;

import lombok.Data;

@Data
public class CourseDTO {
	private Integer courseId;
	private String courseName;
	private String courseDescription;
}
