package com.foxminded.sql.dao.daoimpl.studentdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.foxminded.sql.dao.StudentDao;
import com.foxminded.sql.domain.student.Student;
import com.foxminded.sql.service.support.ConnectionService;

import lombok.Cleanup;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class StudentDaoImpl implements StudentDao<Student> {
	
	private ConnectionService connectionService;

	public StudentDaoImpl(ConnectionService connectionService) {
		this.connectionService = connectionService;
	}

	@Override
	public Optional<Student> get(int id) {
		log.traceEntry(id + "");
		
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement("SELECT student_id, first_name, last_name, group_id FROM students WHERE student_id = ?;");

			statement.setInt(1, id);

			ResultSet response = statement.executeQuery();
			
			if (response.next()) {
				Student student = new Student();
				student.setStudentId(response.getInt("student_id"));
				student.setFirstName(response.getString("first_name"));
				student.setLastName(response.getString("last_name"));
				student.setGroupId(response.getInt("group_id"));
				
				log.traceEntry(student.toString());
				log.traceExit();
				return Optional.of(student);
			}

		} catch (SQLException e) {
			log.catching(e);
		} 
		log.traceExit();
		return Optional.empty();
	}

	@Override
	public List<Student> getAll() {
		List<Student> allStudents = new ArrayList<>();
		
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup Statement statement = conection.createStatement();
			@Cleanup ResultSet response = statement.executeQuery("SELECT student_id, first_name, last_name, group_id FROM students;"); 

			while (response.next()) {
				Student students = new Student();
				students.setStudentId(response.getInt("student_id"));
				students.setFirstName(response.getString("first_name"));
				students.setLastName(response.getString("last_name"));
				students.setGroupId(response.getInt("group_id"));
				allStudents.add(students);
			}
		} catch (SQLException e) {
			log.catching(e);
		}
		log.traceEntry(allStudents.toString());
		log.traceExit();
		return allStudents;
	}

	@Override
	public Optional<Student> save(Student student) {
		log.traceEntry(student.toString());
		try {
			String quary = "INSERT INTO students(student_id, first_name, last_name, group_id) VALUES (nextval('seq_students'), ?, ?, ?);";
			
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement(quary, Statement.RETURN_GENERATED_KEYS);
					
			statement.setString(1, student.getFirstName());
			statement.setString(2, student.getLastName());
			statement.setInt(3, student.getGroupId());
			
			statement.executeUpdate();
			
			@Cleanup ResultSet generatedKeys = statement.getGeneratedKeys();

			if (generatedKeys.next()) {
				Student studentResponse = new Student();
				studentResponse.setStudentId(generatedKeys.getInt("student_id"));
				
				log.traceEntry(student.toString());
				log.traceExit();
				return Optional.of(studentResponse);
			}

		} catch (SQLException e) {
			log.catching(e);
			return Optional.empty();
		} 
		log.traceExit();
		return Optional.empty();
	}

	@Override
	public Optional<Student> update(Student update) {
		log.traceEntry(update.toString());
		try {
			String quary = "UPDATE students SET first_name=?, last_name=?, group_id=? WHERE student_id=?;";
			
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement(quary);

			statement.setString(1, update.getFirstName());
			statement.setString(2, update.getLastName());
			statement.setInt(3, update.getGroupId());
			statement.setInt(4, update.getStudentId());

			int updatedRows = statement.executeUpdate();
			
			if (updatedRows > 0) {
				log.traceEntry(update.toString());
				log.traceExit();
				return Optional.of(update);
			}
			
			log.debug("Nothing to Update");
		} catch (SQLException e) {
			log.catching(e);
		} 
		log.traceExit();
		return Optional.empty();
	}

	@Override
	public void delete(int id) {
		log.traceEntry(id + "");
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement("DELETE FROM students WHERE student_id=?;");

			statement.setInt(1, id);
			
			statement.execute();

		} catch (SQLException e) {
			log.warn("Can't delete entity" + e);
		}
		log.traceExit();
	}
	
	@Override
	public void removeStudentFromCourse(int entityId, int otherEntityId) {
		log.traceEntry(entityId + " " + otherEntityId);
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement("DELETE FROM students_courses WHERE student_id=? AND course_id=?;");

				statement.setInt(1, entityId);
				statement.setInt(2, otherEntityId);
				
				statement.execute();

			} catch (SQLException e) {
				log.warn("Can't delete entity" + e);
			}
		log.traceExit();
	}
	
	@Override
	public void addStudentsToCourse(int courseId, List<Student> students) {
		log.traceEntry(courseId + students.toString());
		
		String quary = createInsertQuaryForList(students.size());
		System.out.println(quary);
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement(quary);
			
			int countRow = 0;
			for (int i = 0; i < students.size(); i++) {
				statement.setInt(++countRow, students.get(i).getStudentId());
				statement.setInt(++countRow, courseId);
			}
				statement.execute();
			
		} catch (SQLException e) {
			log.catching(e);
		}
		log.traceExit();
	}
		
	@Override
	public List<Student> getAllStudentsFromCourse(int courseId) {
		log.traceEntry(courseId + " ");
		List<Student> allStudents = new ArrayList<>();
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement("SELECT student_id FROM students_courses WHERE course_id=?;");
			
			statement.setInt(1, courseId);
			
			@Cleanup ResultSet response = statement.executeQuery();

			while (response.next()) {
				Student student = new Student();
				int studentId = response.getInt("student_id");
				student.setStudentId(studentId);
				allStudents.add(student);
			}
			
		} catch (SQLException e) {
			log.catching(e);
		}
		log.traceExit();
		return allStudents;
	}
	
	@Override
	public List<Student> getAllStudentsFromCourseByName(String courseName) {
		List<Student> allStudents = new ArrayList<>();
		log.traceEntry(courseName);
		String quary = "SELECT students.student_id, students.first_name, students.last_name, students.group_id\r\n" + 
					   "FROM ((students\r\n" + 
					   "INNER JOIN students_courses ON students.student_id = students_courses.student_id)\r\n" + 
					   "INNER JOIN courses ON courses.course_id=students_courses.course_id AND courses.course_name=?);";
		try {
			@Cleanup Connection conection = connectionService.getConnection();
			@Cleanup PreparedStatement statement = conection.prepareStatement(quary);
			
			statement.setString(1, courseName);
			
			@Cleanup ResultSet response = statement.executeQuery();

			while (response.next()) {
				Student student = new Student();
				
				int studentId = response.getInt("student_id");
				int groupId = response.getInt("group_id");
				String firstName = response.getString("first_name");
				String lastName = response.getString("last_name");
				
				student.setStudentId(studentId);
				student.setGroupId(groupId);
				student.setFirstName(firstName);
				student.setLastName(lastName);
				
				allStudents.add(student);
			}
			
		} catch (SQLException e) {
			log.catching(e);
		}
		log.traceExit();
		return allStudents;
	}

	
	private String createInsertQuaryForList(int count) {
		StringBuilder result = new StringBuilder("INSERT INTO students_courses(student_id, course_id) VALUES ");
		for (int i = 0; i < count; i++) {
			result.append("(?, ?)");
			if (i == count-1) {
				result.append(";");
			} else {
				result.append(",");
			}
			result.append(System.lineSeparator());
		}
		
		return result.toString();
		
	}
}
