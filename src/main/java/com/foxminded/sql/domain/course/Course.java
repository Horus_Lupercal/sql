package com.foxminded.sql.domain.course;

import lombok.Data;

@Data
public class Course {
	private Integer courseId;
	private String courseName;
	private String courseDescription;
}
